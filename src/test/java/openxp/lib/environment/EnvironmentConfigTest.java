package openxp.lib.environment;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.webcompere.systemstubs.environment.EnvironmentVariables;
import uk.org.webcompere.systemstubs.jupiter.SystemStub;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;

@ExtendWith(SystemStubsExtension.class)
class EnvironmentConfigTest {

    @SystemStub
    private EnvironmentVariables environmentVariables = new EnvironmentVariables();

    Logger LOG = LoggerFactory.getLogger(this.getClass().getName());
    private EnvironmentConfig environmentConfig;

    @ParameterizedTest
    @CsvSource( {"ENVIRONMENT,''", "ENVIRONMENT,' '", "' ENVIRONMENT', 'test'"})
    void testAssertionErrorWhenInvalidEnvironment(String name, String value) throws Exception {
        environmentVariables.set(name, value)
            .execute( () -> {
                Assertions.assertThrows(AssertionError.class, EnvironmentConfig::new);
            });
    }

    @ParameterizedTest
    @CsvSource({"PROD,prod_", "TEST,test_"})
    void testThatEnvironmentIsLowerCasedAndPostFixed(String input, String expected) throws Exception {
        environmentVariables.set("ENVIRONMENT", input)
            .execute( () -> {
                environmentConfig = new EnvironmentConfig();
                Assertions.assertEquals(expected, environmentConfig.getEnvironmentPrefix());
            });
    }

    @ParameterizedTest
    @CsvSource({"TEST,prop1,test-prop1", "PROD,prop1,prod-prop1", "NA,prop1,default-prop1"})
    void testThatEnvironmentPropertyValuesAreResolvedCorrectly(String env, String input, String expected) throws Exception{
        environmentVariables.set("ENVIRONMENT", env)
            .execute( () -> {
                environmentConfig = new EnvironmentConfig();
                String propertyValue = environmentConfig.getProperty(input);
                Assertions.assertEquals(expected, propertyValue);
            });
    }

    @Test
    void testNonExistingEnvironmentWithNoDefaultPropShouldReturnPropName() throws Exception{
        final String nonExsistingEnvironment = "NA";
        final String propWithNoDefault = "prop2-nodefault";
        environmentVariables.set("ENVIRONMENT", nonExsistingEnvironment)
            .execute( () -> {
                environmentConfig = new EnvironmentConfig();
                String propertyValue = environmentConfig.getProperty(propWithNoDefault);
                Assertions.assertEquals(propWithNoDefault, propertyValue);
            });
    }

    @Test
    void testThatPropertyNameIsReturnedWhenPropertyDontExist() throws Exception{
        environmentVariables.set("ENVIRONMENT", "TEST")
            .execute( () -> {
                environmentConfig = new EnvironmentConfig();
                String nonExistingPropertyName = "noProp";
                Assertions.assertEquals(nonExistingPropertyName, nonExistingPropertyName);
            });
    }

    @ParameterizedTest
    @CsvSource({"TEST,prop3,''", "PROD,prop3,prod-prop3", "NA,prop3,default-prop3"})
    void testThatBlankPropValueIsAllowedForTestEnvironment(String env, String input, String expected) throws Exception{
        environmentVariables.set("ENVIRONMENT", env)
            .execute( () -> {
                environmentConfig = new EnvironmentConfig();
                String propertyValue = environmentConfig.getProperty(input);
                Assertions.assertEquals(expected, propertyValue);
                if ("TEST".equals(env)){
                    Assertions.assertTrue(propertyValue.isBlank());
                }
            });
    }

    @Test
    void testThatWhitespaceInPropertyValueIsNotRemoved() throws Exception{
        final String expected = "Do not trim    extra space    in properties but trim start and end";
        environmentVariables.set("ENVIRONMENT", "NA")
            .execute( () -> {
                environmentConfig = new EnvironmentConfig();
                String propertyValue = environmentConfig.getProperty("prop4");
                Assertions.assertEquals(expected, propertyValue);
            });
    }
}