package openxp.lib.environment;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;


class EnvironmentPropertiesTest {

    @Test
    void testLoadExistingPropertyFileAndLoadDefaultProperty(){
        final String existingPropertyName = "prop1";
        final String existingPropertyNameValue = "default-prop1";
        EnvironmentProperties environmentProperties = new EnvironmentProperties("environment.properties");
        Assertions.assertEquals(existingPropertyNameValue, environmentProperties.get(existingPropertyName));
    }

    @Test
    void testRuntimeExceptionWhenLoadingInvalidPropertiesFile(){
        final String nonExistingPropertyFileName = "no.properties";
        Assertions.assertThrows(RuntimeException.class, () -> new EnvironmentProperties(nonExistingPropertyFileName));
    }

}