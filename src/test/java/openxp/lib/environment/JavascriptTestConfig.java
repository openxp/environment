package openxp.lib.environment;

import com.enonic.xp.testing.ScriptRunnerSupport;
import org.junit.jupiter.api.Test;

public class JavascriptTestConfig
        extends ScriptRunnerSupport
{
    @Override
    public String getScriptTestFile()
    {
        return "openxp/lib/environment/index.js";
    }

}
