package openxp.lib.environment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EnvironmentProperties {
    private final Logger LOG = LoggerFactory.getLogger(EnvironmentProperties.class);
    private final Properties properties = new Properties();

    public EnvironmentProperties(String propertiesFilename){

        try {
            listFiles();
            //properties.load(EnvironmentConfig.class.getResourceAsStream(propertiesFilename));
        }catch (Exception e){
            LOG.error("No {} file in " + EnvironmentConfig.class.getPackage(), propertiesFilename);
            //throw new RuntimeException(propertiesFilename + " file not found in " + EnvironmentProperties.class.getPackage());
        }
    }
    public Set<String> listFilesUsingJavaIO(String dir) {
        return Stream.of(new File(dir).listFiles())
                .filter(file -> !file.isDirectory())
                .map(File::getName)
                .collect(Collectors.toSet());
    }

    private void listFiles() throws Exception {
        LOG.info("LIST ENVIRONMENT VARIABLE FILES 5");
        try {
            URL res = EnvironmentConfig.class.getResource("env.properties");
            LOG.info("res.toString() " + res.toString());
            LOG.info(res.getFile());
            LOG.info(res.getPath());
            Set<String> files = listFilesUsingJavaIO(res.getFile());
            while (files.iterator().hasNext()){
                LOG.info(files.iterator().next());
            }

        }catch (Exception e){
            LOG.error(e.getMessage());
        }
    }

    public Object get(String key){
        return properties.get(key);
    }
}
