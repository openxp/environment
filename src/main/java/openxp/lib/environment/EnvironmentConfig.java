package openxp.lib.environment;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true)
public class EnvironmentConfig {
    private final Logger LOG = LoggerFactory.getLogger(EnvironmentConfig.class);
    private String environmentPrefix;
    private String environment;
    private final String propertiesFilename = "environment.properties";
    private final EnvironmentProperties environmentProperties = new EnvironmentProperties(propertiesFilename);


    public EnvironmentConfig() {
        environment = System.getenv("ENVIRONMENT");
        if (environment == null || environment.isBlank()) {
            //throw new AssertionError("ENVIRONMENT not set as environment variable");
            LOG.error("ENVIRONMENT not set as environment variable. Will only use default non-environment variables");
        }
    }

    protected String getEnvironmentPrefix(){
        if (environmentPrefix == null && environment != null && !environment.isEmpty()) {
            environmentPrefix = (environment + "_").toLowerCase();
        }
        return environmentPrefix;
    }

    /**
     * Returns propertyName if property does not exist in property file
     * */
    public String getProperty(String propertyName){
        if (propertyName == null || "".equals(propertyName)) {
            LOG.warn("propertyName null or empty");
            return propertyName;
        }
        String property = (String) environmentProperties.get(getEnvironmentPrefix() + propertyName);
        if (property == null) { //No environment specific property
            property = (String) environmentProperties.get(propertyName);
            if (property == null) {
                LOG.warn("property is null in in EnvConfig.getProperty({}). Returning propertyName", propertyName);
                return propertyName;
            }
        }
        return property.trim();
    }
}

