const environmentConfig = __.newBean('openxp.lib.environment.EnvironmentConfig');

function getProperty(propertyName){
    return environmentConfig.getProperty(propertyName);
}
exports.getProperty = getProperty;