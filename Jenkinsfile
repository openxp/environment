@Library('gje-pipeline-library@master') _

def GROUP = null
def PROJECT_NAME = null
def PROJECT_VERSION = null

def windowsJdkVersion = "jdk11.0.2"

pipeline {
    agent none
    options {
        buildDiscarder(logRotator(numToKeepStr: '10'))
        timeout(time: 60, unit: 'MINUTES')
        ansiColor colorMapName: 'XTerm'
    }

    tools {nodejs "node-current"}

    environment {
        PROXY_AUTH = credentials('http-proxy')
        HTTPS_PROXY = "http://{env.PROXY_AUTH}@${no.gjensidige.pipeline.Constants.WEBGUARD_PROXY_URI}:${no.gjensidige.pipeline.Constants.WEBGUARD_PROXY_PORT}"
        HTTP_PROXY = "http://{env.PROXY_AUTH}@${no.gjensidige.pipeline.Constants.WEBGUARD_PROXY_URI}:${no.gjensidige.pipeline.Constants.WEBGUARD_PROXY_PORT}"
        NO_PROXY="localhost,127.0.0.1,git,nexus,svn,mvn,.mistraldev.com,.mistral.mistralnett.com"
        NON_PROXY = "localhost|127.0.0.1|git|nexus|svn|mvn|*.mistraldev.com|*.mistral.mistralnett.com"

        JAVA_TOOL_OPTIONS="-Dhttp.proxyHost=${no.gjensidige.pipeline.Constants.WEBGUARD_PROXY_URI} " +
                "-Dhttp.proxyPort=${no.gjensidige.pipeline.Constants.WEBGUARD_PROXY_PORT} " +
                "-Dhttps.proxyHost=${no.gjensidige.pipeline.Constants.WEBGUARD_PROXY_URI} " +
                "-Dhttps.proxyPort=${no.gjensidige.pipeline.Constants.WEBGUARD_PROXY_PORT} " +
                "-Dhttp.nonProxyHosts=${NON_PROXY} " +
                "-Dhttps.nonProxyHosts=${NON_PROXY} " +
                "-Dhttps.proxyUser=${$PROXY_AUTH_USR} -Dhttps.proxyPassword=${$PROXY_AUTH_PSW} " +
                "-Dhttp.proxyUser=${$PROXY_AUTH_USR} -Dhttp.proxyPassword=${$PROXY_AUTH_PSW}"

        npmRegistry = 'config set registry http://nexus.mistraldev.com/content/repositories/npmgroup/'
    }

    stages {

        stage('prepare') {
            agent {label 'windows'}
            steps {
                checkout scm
                script {
                    notifyBitbucket()
                    def props = readProperties  file: 'gradle.properties'

                    GROUP = props['group']
                    PROJECT_NAME = props['projectName']
                    PROJECT_VERSION = props['version']
                }
                bat "curl --version" // check curl version - important (bug related)
            }
        }

        stage('clean') {
            agent {label 'windows'}
            tools {jdk "${windowsJdkVersion}"}
            steps {
                bat "gradlew clean" // is a necessity after version changes
            }
        }

        stage('build') {
            agent {label 'windows'}
            tools {jdk "${windowsJdkVersion}"}
            steps {
                bat "gradlew build -x test"
                stash includes: 'src/**/*', name: 'src'
            }
        }


        stage('server: unit test') {
            agent {label 'windows'}
            tools {jdk "${windowsJdkVersion}"}
            steps {
                bat "gradlew test"
            }
        }

        stage('nexus') {
            agent {label 'windows'}
            tools {jdk "${windowsJdkVersion}"}
            steps {
                lock('maven-publish') {
                    print "Publish to nexus"
                    withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                                      credentialsId   : 'nexus-maven-publish',
                                      usernameVariable: 'USERNAME',
                                      passwordVariable: 'PASSWORD']]) {
                        bat "gradlew publish -PnexusUsername=${USERNAME} -PnexusPassword=${PASSWORD}"
                    }
                }
            }
        }

        stage('SonarQube analysis') {
            agent {
                docker {
                    image 'emeraldsquad/sonar-scanner'
                }
            }
            environment {
                JAVA_HOME = "/usr/lib/jvm/java-1.8-openjdk"
                PATH = "${JAVA_HOME}/bin:${PATH}"
            }
            steps {
                unstash 'src'
                script {
                    sonarAnalysis {
                        projectKey = "${GROUP}:${PROJECT_NAME}"
                        projectName = PROJECT_NAME
                        projectVersion = PROJECT_VERSION
                        sources = 'src/main'
                        additionalOpts = '-Dsonar.java.binaries=src -Dsonar.javascript.file.suffixes=.js,.jsx,.es6,.ts,.tsx'
                    }
                }
            }
        }
    }

    post {
        always {
            node('master') {
                script {
                    currentBuild.result = currentBuild.result ?: 'SUCCESS'
                    notifyBitbucket()
                }
            }
            notifyDeveloper()
        }
    }

}

def notifyDeveloper() {
    // send email to developers
    emailext(
            subject: "JENKINS Job -> ${currentBuild.result}: ${currentBuild.fullDisplayName}",
            body: '''${SCRIPT, template="groovy-html.template"}''',
            attachLog: true,
            mimeType: 'text/html',
            recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']]
    )
}