package openxp.environment

import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.execution.TaskExecutionGraph
import org.gradle.api.execution.TaskExecutionGraphListener
import org.gradle.api.execution.TaskExecutionListener
import org.gradle.api.invocation.Gradle
import org.gradle.api.provider.Property
import org.gradle.api.tasks.TaskState

interface EnvironmentPluginExtension {
    Property<String> getEnvironments()
}

class EnvironmentPlugin
    implements Plugin<Project>{

    private Project project
    static int counter = 0;

    void apply(Project project) {
        project.gradle.taskGraph.addTaskExecutionListener( new TaskInterceptor())
        def extension = project.extensions.create('openxp', EnvironmentPluginExtension)
        this.project = project

        /*if (project.hasProperty("openxp")){
            project.getExtensions().findByName("openxp").properties.forEach( prop -> println(prop))
            println 'environments'
            println "${extension.environments.get()}"
        }*/

        String resourcesPath = 'src/main/resources'
        String environmentFilePath = 'openxp/lib/environment'
        String environmentFileName = project.group + '.' + project.name + '.env.properties'
        //Create directory which property files should reside in
        boolean createdDirectory = new File(project.file(resourcesPath), environmentFilePath).mkdirs()
        if (createdDirectory) println('Created directory for environment file')
        //Create file in new directory
        boolean createdEnvironmentFile = new File(project.file(resourcesPath + '/' + environmentFilePath), environmentFileName).createNewFile()
        if (createdEnvironmentFile) println('Created environment file ' + resourcesPath + '/' + environmentFilePath + '/' + environmentFileName)
        new File(project.file(resourcesPath + '/' + environmentFilePath), 'env.properties').createNewFile()

    }
}
