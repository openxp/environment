package openxp.environment

import groovy.util.logging.Slf4j
import org.gradle.api.DefaultTask
import org.gradle.api.Task
import org.gradle.api.execution.TaskExecutionListener
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskState

import java.util.function.BiConsumer

@Slf4j
class TaskInterceptor implements TaskExecutionListener{
    @Override
    void beforeExecute(Task task) {
        if (!"jar".equals(task.name)) return
        println "DO IMPORTANT STUFF (jar)"
    }

    @Override
    void afterExecute(Task task, TaskState taskState) {
        if (!"processResources".equals(task.name)) return
        String openxpFilePath = 'resources/main/openxp/lib/environment'
        File openxpBuildFilePath = new File(task.project.buildDir , openxpFilePath)
        println 'list files in ' + openxpBuildFilePath.path
        File[] files = openxpBuildFilePath.listFiles();
        files.each {println it.name}
        files.eachWithIndex{ File entry, int i -> println 'test' + entry.name}
        println "DO IMPORTANT STUFF (processResorces)"
        /*println task.outputs.files.forEach(f -> {
            println 'create file path: ' + f.path
            new File(f.path, 'test.txt').createNewFile()
        })*/
    }
}

class CopyTask extends DefaultTask {
    @TaskAction
    void run(){
        println "RUN COPY TASK"
    }
}
