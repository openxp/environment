# Enonic XP environment library

Support environment variables in applications.

For this library to work an `%ENVIRONMENT%` environment variable needs to be available for the running Enonic XP instance. 
This is retrieved in the code with System.getenv("ENVIRONMENT") which is immutable in the jvm.

Minumum XP version 7.0.0

## In build.gradle

```groovy
dependencies {
    include "openxp.lib:environment:2.0.0"
}
repositories {
    maven {
        url 'https://openxp.jfrog.io/artifactory/public'
    }
}
```

## In javascript

```javascript
var environmentLib = require('/lib/openxp/environment');
var myEnvironmentProperty = environmentLib.getProperty('myEnvironmentProperty');
```

## Add environment file

Add the file `resources/openxp/lib/environment/environment.properties` in your app.
Add properties, example:

```properties
myEnvironmentProperty = someValue
dev_myEnvironmentProperty = someValueForDevEnvironment
test_myEnvironmentProperty = someValueForTestEnvironment
prod_myEnvironmentProperty = someValueForProdEnvironment
```

Which value is chosen depends on the value of case-insensitive `%ENVIRONMENT%` property set for this Enonic XP instance.

Note:
- If there is no value for the active environment, the default value will be returned. 
- If there is no existing property with given name, the propertyName will be returned.



## Publishing (only for jfrog openxp administrator)

Set artifactoryUser and artifactoryToken in .gradle/gradle.properties. 

```bash
./gradlew clean build artifactoryPublish 
```

## Versions

1.0.0 
   
- Initial version. Working but missing functioning tests

1.0.1 

- Move to github. Jenkins pipeline. Gradle upgrade. Misc cleanup.

1.0.2 

- Fix and move log warning that environment is not set

1.0.3

- Documentation. Move to bitbucket and jfrog artifactory

2.0.0 

- Move to bitbucket openxp repository
- Publish to JFrog
- Refactor code and add tests
- Make ready for Enonic Market